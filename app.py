from flask import Flask
from flask_socketio import SocketIO, join_room, leave_room, send

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins='*')  # ['http://127.0.0.1:5000'])


@app.route('/<name>')
def send_message_to_room(name):
    message = 'This message is for {}'.format(name)
    socketio.emit('after connect', {'data': message}, room=name)
    return "Message sent to user... Check the page!"


@socketio.on('connect')
def test_connect():
    print("Connecting...")
    socketio.emit('after connect', {'data': 'Does it work? Of course!'})


@socketio.on('my event')
def handle_my_custom_event(json):
    print('received json: ' + str(json))


@socketio.on('join')
def on_join(data):
    """
    Group users into rooms based on their first username letter
    """
    username = data['username']
    print(data)
    print(username)
    join_room(username)
    socketio.emit('after connect', {'data': 'User connected to room'}, room=username)
    # send(username + ' has entered the room.', room=room)



if __name__ == '__main__':
    socketio.run(app)  # replaces app.run()
