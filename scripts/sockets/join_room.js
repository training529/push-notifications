var socket = io.connect('http://localhost:5000');

function joinRoom() {
    var user = document.getElementById("user").value;
    socket.emit('join', {username: user});
    socket.on('after connect', function(connectMsg) {
        $('#initial_message').append('<br>' + $('<div/>').text(connectMsg.data).html());
    });
}
